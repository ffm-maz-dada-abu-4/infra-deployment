resource "kubernetes_deployment_v1" "attendance" {
  metadata {
    name = "attendance"
    labels = {
      test = "attendance"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        test = "attendance"
      }
    }

    template {
      metadata {
        labels = {
          test = "attendance"
        }
      }

      spec {
        container {
          image = "512134834387.dkr.ecr.us-east-1.amazonaws.com/ffm-attendance-service-4:latest"
          name  = "attendance"

          resources {
            limits = {
              cpu    = "1"
              memory = "1G"
            }
            requests = {
              cpu    = ".5m"
              memory = "512Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "attendance_svc" {
  metadata {
    name = "attendance"
  }
  spec {
    selector = {
      test = kubernetes_deployment_v1.attendance.metadata.0.labels.test
    }
    
    port {
      port        = 3000
      target_port = 3000
    }

    type = "NodePort"
  }
}
